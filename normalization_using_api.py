import json

import requests
from openpyxl import load_workbook


def json_to_py(file_name):
    with open(file_name) as file:
        data = json.load(file)
    
    return data


def api_call(url, headers, data):
    response = requests.post(url, headers=headers, json=data)
    data = response.json()
    result = data.get("data").get("items")

    return result

model_item_master = json_to_py("model_item_master.json")

def match_medicine_names():
    url = "https://api.pharmacyone.io/prod/mobile_search_bill_item"
    headers = {"session-token": "wantednote", "Content-Type": "application/json"}

    matched_medicine_names = []
    api_mrp = []
    model_item_master_mrp = []
    api_manufacturer_names = []
    model_item_master_manufacturer_names = []
    unmatched_medicine_names = []
    unmatched_mrp = []
    unmatched_manufacturer_names = []
    called_medicine_names = []

    for item1 in model_item_master:
        if item1.get("name") in called_medicine_names:
            continue
        data = {"search": item1.get("name")}
        api_call_list = api_call(url, headers, data)
        called_medicine_names.append(item1.get("name"))

        for item2 in api_call_list:
            if item2.get("name")  == item1.get("name"):
                matched_medicine_names.append(item1.get("name"))
                api_mrp.append(item2.get("mrp"))
                model_item_master_mrp.append(item1.get("mrp"))
                api_manufacturer_names.append(item2.get("manufacturer"))
                model_item_master_manufacturer_names.append(item1.get("manufacturer"))
            else:
                unmatched_medicine_names.append(item1.get("name"))
                unmatched_mrp.append(item1.get("mrp"))
                unmatched_manufacturer_names.append(item1.get("manufacturer"))

                
    return matched_medicine_names, unmatched_medicine_names, unmatched_mrp, unmatched_manufacturer_names, api_mrp, model_item_master_mrp, api_manufacturer_names, model_item_master_manufacturer_names

matched_medicine_names, unmatched_medicine_names, unmatched_mrp, unmatched_manufacturer_names, api_mrp, model_item_master_mrp, api_manufacturer_names, model_item_master_manufacturer_names = match_medicine_names()

# Write to workbook
def write_to_excel(workbook_name, matched_worksheet_name, unmatched_worksheet_name):
    workbook = load_workbook(workbook_name)
    matched_worksheet = workbook[matched_worksheet_name]
    unmatched_worksheet = workbook[unmatched_worksheet_name]
    matched_worksheet.append(["matched_medicine_name", "api_mrp", "model_item_master_mrp", "api_manufacturer_name", "model_item_master_manufacturer_name"])
    unmatched_worksheet.append(["unmatched_medicine_name", "mrp", "manufacturer"])

    for i in range(len(matched_medicine_names)):
        matched_worksheet.append([matched_medicine_names[i], api_mrp[i], model_item_master_mrp[i], api_manufacturer_names[i], model_item_master_manufacturer_names[i]])

    for i in range(len(unmatched_medicine_names)):
        unmatched_worksheet.append([unmatched_medicine_names[i], unmatched_mrp[i], unmatched_manufacturer_names[i]])

    workbook.save(workbook_name)

    return True

result = write_to_excel("normalized_data_2.xlsx", "matched_data", "unmatched_data")

